# image-triangles

Makes a [voronoi diagram](https://en.wikipedia.org/wiki/Voronoi_diagram) and fills each cell with the average color the image below in.

### examples
![Sierra mountains original](examples/inputs/sierra.jpeg)
![Sierra mountains post-filter](examples/outputs/sierra.jpeg)
![Hawaii original](examples/inputs/birds-eye-view.png)
![Hawaii post-filter](examples/outputs/birds-eye-view.png)
![Dog original](examples/inputs/luna.jpeg)
![Dog post-filter](examples/outputs/luna.jpeg)


### to run:

#### with nix

```
nix run  --experimental-features "nix-command flakes" ".#" -- --minDistance 0.02 --input examples/inputs/birds-eye-view.png --output output.jpeg
```

#### with cabal
Install [cabal & ghc](https://www.haskell.org/ghcup/) if you don't have them.

```
cabal update
cabal run image-triangles -- --minDistance 0.02 --input examples/inputs/birds-eye-view.png --output output.jpeg
```

