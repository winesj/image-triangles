#! /usr/bin/env nix
#! nix shell "nixpkgs#findutils"
nix build;
ls examples/inputs | xargs -I aoeui --max-procs=0 ./result/bin/image-triangles --minDistance 0.01 --input ./examples/inputs/aoeui --output examples/outputs/aoeui
