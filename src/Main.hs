module Main where

import Control.Arrow
import Control.Monad qualified as M
import Control.Monad.Parallel qualified as MP
import Control.Monad.Zip (MonadZip (mzipWith))
import Control.Parallel.Strategies
import Data.Bifunctor qualified as Bi
import Data.Colour qualified as C
import Data.Colour.Names qualified as CN
import Data.Colour.RGBSpace (uncurryRGB)
import Data.Colour.SRGB qualified as CL
import Data.Colour.SRGB.Linear qualified as CL
import Data.List
import Data.List qualified as L
import Data.Map qualified as M
import Data.Massiv.Array qualified as M
import Data.Massiv.Array qualified as Ma
import Data.Massiv.Array.IO qualified as M
import Data.Maybe qualified as My
import Data.Set qualified as S
import Data.Vector.Unboxed qualified as Vec
import Debug.Trace
import Debug.Trace qualified as D
import Debug.Trace qualified as DT
import Debug.Trace qualified as T
import Diagrams qualified as DP
import Diagrams.Backend.Rasterific
import Diagrams.Backend.Rasterific.CmdLine
import Diagrams.Prelude as D
import GHC.Generics
import Graphics.Color.Space qualified as Co
import MinDistanceSample qualified as MDS
import Options.Generic
import System.Environment qualified as Env
import System.Random
import System.Random.Internal
import System.Random.SplitMix
import Triangles (getTriangleAverageRGB)
import Triangles qualified as Tri

toColour :: (Fractional a) => Co.Color (Co.SRGB Co.Linear) a -> Colour a
toColour (Co.ColorSRGB r g b) = CL.rgb r g b

corners :: [(Double, Double)]
corners = (,) <$> [0, 1] <*> [0, 1]

shapeCircumference :: [Point V2 Double] -> Double
shapeCircumference = Data.List.sum . map D.norm . loopOffsets . fromVertices

genImage :: M.Image M.S (Co.SRGB 'Co.Linear) Double -> V2 Double -> Double -> StdGen -> QDiagram Rasterific V2 Double Any
genImage image dimensionsVec minDistance gen =
  scaleX widthHeightRatio
    . reflectY
    . rectEnvelope (mkP2 0 0) (1 ^& 1)
    . mconcat
    . map ((\x -> drawVoronoiRegion x <> overlayEdges x) . uncurry Tri.voronoiDiagramCorners)
    $ voronoi
  where
    overlayEdges =
      lw (local 0.001)
        . lc black
        . strokeLocLoop
        . fromVertices

    drawVoronoiRegion shape =
      lw 0
        . fillColor (Tri.voronoiRegionAverageColor image dimensions shape)
        . strokeLocLoop
        . fromVertices
        $ shape

    widthHeightRatio :: Double
    widthHeightRatio = (dimensionsVec ^. _x) / (dimensionsVec ^. _y)

    dimensions :: (Int, Int)
    dimensions = (ceiling $ dimensionsVec ^. _x, ceiling $ dimensionsVec ^. _y)

    singleVoronoi = last voronoi

    voronoi = map (\x -> (x, Tri.nClosestPoints 20 x corners')) corners'

    averageSideSize = (fromIntegral (uncurry (+) dimensions)) / 2

    padding = (/ 10) <$> V2 1 1

    corners' :: [P2 Double]
    corners' =
      map (p2 . Bi.first (/ widthHeightRatio) . unp2 . (.-^ ((/ 2) <$> padding)))
        . MDS.randomPoints ((mkP2 widthHeightRatio 1) .+^ padding) (minDistance * widthHeightRatio)
        $ gen

deriving instance Generic (CL.RGB a)

deriving instance (NFData a) => NFData (CL.RGB a)

toDimensionVector :: (Ma.Size r, Fractional a) => Ma.Array r Ma.Ix2 e -> V2 a
toDimensionVector image =
  p2 (fromIntegral cols, fromIntegral rows) .-. p2 (0.0, 0.0)
  where
    (M.Sz2 rows cols) = Ma.size image

data CLIOptions = CLIOptions
  { input :: FilePath,
    output :: FilePath,
    minDistance :: Double
  }
  deriving (Generic, ParseRecord)

main :: IO ()
main = do
  CLIOptions {..} <- getRecord "image options"
  gen' <- getStdGen
  print gen'
  image :: M.Image M.S (Co.SRGB 'Co.Linear) Double <- M.readImageAuto input
  let dims = toDimensionVector image
  renderRasterific output (D.dims dims) (genImage image dims minDistance gen')
