module Triangles where

import Control.Arrow
import Control.Parallel.Strategies
import Data.Array qualified as A
import Data.Colour qualified as C
import Data.Colour.Names qualified as C
import Data.Colour.SRGB.Linear (Colour)
import Data.Colour.SRGB.Linear qualified as C
import Data.Colour.SRGB.Linear qualified as CL
import Data.Fixed
import Data.Function qualified as F
import Data.List
import Data.List qualified as L
import Data.Map qualified as M
import Data.Massiv.Array qualified as Ma
import Data.Massiv.Array.IO qualified as Ma
import Data.Maybe
import Data.Ord qualified as O
import Data.PQueue.Prio.Min qualified as PQ
import Data.Ratio
import Diagrams.Prelude
import Data.Set qualified as S
import Data.Vector.Generic.Base (Vector)
import Data.Vector.Generic.Mutable (MVector)
import Data.Vector.Unboxed qualified as Vec
import Data.Vector.Unboxed.Deriving
import Debug.Trace (traceShow)
import Debug.Trace qualified
import Debug.Trace qualified as D
import Diagrams.Trail (trailPoints)
import Diagrams.TwoD
import Data.Heap qualified as H
import Diagrams.TwoD.Path.IntersectionExtras qualified as I
import Diagrams.TwoD.Segment.Bernstein (listToBernstein)
import Graphics.Color.Space qualified as Co
import System.Random
import Data.Containers.ListUtils (nubOrd)

toColour :: (Fractional a) => Co.Color (Co.SRGB Co.Linear) a -> Colour a
toColour (Co.ColorSRGB r g b) = CL.rgb r g b

-- from -0.05 to 1.05 so there aren't missing/elongated triangles at the edges
borderSize :: Double
borderSize = 0.05

randomPoints :: StdGen -> [(Double, Double)]
randomPoints = map (bimap toZeroToOneTuple toZeroToOneTuple) . randomRs ((0 :: Word, 0 :: Word), (maxBound, maxBound))
  where
    toZeroToOneTuple :: Word -> Double
    toZeroToOneTuple x = ((fromIntegral x / (fromIntegral (maxBound :: Word))) * (1 + (2 * borderSize))) - borderSize

combinations :: (Ord b, Floating b, NFData b) => [P2 b] -> [(P2 b, P2 b)]
combinations =
  sortOn (abs . uncurry distanceA)
    . nubOrd
    . filter (uncurry (/=))
    . concat
    . map (\(x : xs) -> takeSortOn 10 (abs . uncurry distanceA) . map (x,) $ xs)
    . init -- last output of tails is empty list
    . tails

toPlanarGraph :: forall n. (NFData n, Floating n, Ord n) => [P2 n] -> [(Point V2 n, Point V2 n)]
toPlanarGraph =
  removeIntersections
    . sortOn (abs . uncurry distanceA)
    . combinations
  where
    removeIntersections :: [(Point V2 n, Point V2 n)] -> [(Point V2 n, Point V2 n)]
    removeIntersections = foldl' addIfNoIntersection []

    addIfNoIntersection xs x
      | all (noIntersection x) xs = x : xs
      | otherwise = xs

    noIntersection l1 l2 = sharedEndPoint || (null $ intersectPointsT (uncurry toLocatedTrail l1) (uncurry toLocatedTrail l2))
      where
        sharedEndPoint = (< 4) . length . nub $ [fst l1, snd l1, fst l2, snd l2]

toLocatedTrail :: (TrailLike a) => Point (V a) (N a) -> Point (V a) (N a) -> Located a
toLocatedTrail p1 p2 = fromVertices [p1, p2] `at` p1

withinShape :: (RealFloat v) => Point V2 v -> [Point V2 v] -> Point V2 v -> Bool
withinShape pointInShape verticies candidate = all ((< quarterTurn) . fmap abs . uncurry signedAngleBetweenDirs) $ zip (shapeDirections pointInShape) (shapeDirections candidate)
  where
    shapeDirections p = map (dirBetween p) verticies

sortOnAngle center = sortOn (normalizeAngle . signedAngleBetweenDirs xDir . dirBetween center)

voronoiDiagramCorners :: (RealFloat n) => Point V2 n -> [Point V2 n] -> [Point V2 n]
voronoiDiagramCorners center midpoints =
  sortOnAngle center
    . filter isValidMidpoint
    . concat
    $ [intersectPointsT l0 l1 | l0 <- tangentTrails, l1 <- tangentTrails]
  where
    lessThanQuarterTurn candidate = candidate <= quarterTurn @@ turn || candidate >= (1 - quarterTurn) @@ turn

    quarterTurn = 0.251

    tangentTrails = map tangentTrail midpoints

    appendHead (x : xs) = xs ++ [x]

    isValidMidpoint candidate = all isNonObtuseMidpoint . filter (/= candidate) $ midpoints
      where
        isNonObtuseMidpoint m =
          lessThanQuarterTurn
            . normalizeAngle
            $ angleBetweenDirs (dirBetween m center) (dirBetween m candidate)

    tangentTrail midpoint = fromVertices [midpoint .-^ tangentVec, midpoint .+^ tangentVec]
      where
        -- implicitly uses the unit vector * 8 as an infinitely long vector
        tangentVec =
          scale 2
            . fromDirection
            . rotateBy (1 / 4)
            $ dirBetween midpoint center



-- real	0m47.306s
-- user	0m45.160s
-- sys	0m0.345s

-- takeSortOn n f = map snd . H.toUnsortedList . H.take n . H.fromList . map (\x -> (f x, x))




-- real	0m43.514s
-- user	0m41.489s
-- sys	0m0.271s

takeSortOn :: Ord a => Int -> (b -> a) -> [b] -> [b]
takeSortOn n f =
  map snd
    . PQ.take n
    . PQ.fromList
    . map (\x -> (f x, x))


-- real	0m44.868s
-- user	0m44.199s
-- sys	0m0.322s
-- takeSortOn :: Ord a => Int -> (b -> a) -> [b] -> [b]
-- takeSortOn n f =
--      take n
--     . sortOn f

nClosestPoints :: (RealFloat n) => Int -> Point V2 n -> [Point V2 n] -> [Point V2 n]
nClosestPoints n p =
  map (pointBetween p)
    . takeSortOn n (abs . distanceA p)
    . filter (/= p)

findVoronoiDiagram :: (RealFloat n) => [(Point V2 n, Point V2 n)] -> [(Point V2 n, [Point V2 n])]
findVoronoiDiagram =
  M.toList
    . M.mapWithKey
      ( \key ->
          L.sortOn (normalizeAngle . signedAngleBetweenDirs xDir . dirBetween key)
            . map (pointBetween key)
            . S.toList
      )
    . adjacencyMapOf

pointBetween :: (Affine p, Fractional a) => p a -> p a -> p a
pointBetween p0 p1 = p0 .+^ ((p1 .-. p0) ^/ 2)

findTriangles :: (Ord b) => [(b, b)] -> S.Set (S.Set b)
findTriangles edges = S.unions . S.map threeCyclesOf . M.keysSet $ adjacencyMap
  where
    threeCyclesOf node =
      S.unions
        . S.map (\x -> S.map (\y -> S.fromList [node, x, y]) $ S.delete node . S.intersection originalNodeNeighbors . (M.!) adjacencyMap $ x)
        $ originalNodeNeighbors
      where
        originalNodeNeighbors = fromMaybe S.empty (adjacencyMap M.!? node)

    adjacencyMap = adjacencyMapOf edges

adjacencyMapOf :: Ord b => [(b, b)] -> M.Map b (S.Set b)
adjacencyMapOf edges = M.fromListWith S.union . map (second S.singleton) $ (edges ++ edgesReversed)
  where
    edgesReversed = map (\(a, b) -> (b, a)) edges

triangleAdjacencyMap :: (Ord b) => S.Set (S.Set b) -> M.Map b (S.Set (S.Set b))
triangleAdjacencyMap s = M.fromListWith S.union . concatMap (\s' -> map (,S.singleton s') . S.toList $ s') $ S.toList s

getPointsInTriangle :: p -> S.Set (P2 Int) -> [(Int, Int)]
getPointsInTriangle image pts =
  S.toList . S.unions . map S.fromList $
    [ ptsBtween (makeLine p1 p3) (makeLine p1 p2),
      ptsBtween (makeLine p1 p3) (makeLine p2 p3),
      ptsBtween (makeLine p1 p2) (makeLine p2 p3)
    ]
  where
    [p1, p2, p3] = sortOn fst . map unp2 . S.toList $ pts

blendEqually :: (Ord a, Floating a) => [Colour a] -> Colour a
blendEqually colors = C.affineCombo (map (fraction,) colors) C.white
  where
    fraction = 1.0 / (fromIntegral . length $ colors)

voronoiRegionAverageColor :: (Integral a, Integral b) => Ma.Image Ma.S (Co.SRGB 'Co.Linear) Double -> (a, b) -> [P2 Double] -> Colour Double
voronoiRegionAverageColor image (x', y') =
  blendEqually
    . concatMap (getColorsInTriangle image (x', y'))
    . filter ((== 3) . S.size)
    . map (S.fromList . take 3)
    . tails
    . nubOrd
    . map scaleToImageCoords
  where
    scaleToImageCoords :: P2 Double -> P2 Int
    scaleToImageCoords p = round <$> p2 (fromIntegral x' * p ^. _x, fromIntegral y' * p ^. _y)

    scaleToUnitCoords :: P2 Int -> P2 Double
    scaleToUnitCoords p = p2 ((fromIntegral x' / (fromIntegral $ p ^. _x)), fromIntegral y' / (fromIntegral $ p ^. _y))

getColorsInTriangle :: Ma.Image Ma.S (Co.SRGB 'Co.Linear) Double -> (a, b) -> S.Set (P2 Int) -> [Colour Double]
getColorsInTriangle image (x', y') triangle = mapMaybe index' points
  where
    points :: [(Int, Int)]
    points = getPointsInTriangle image triangle

    index' :: (Int, Int) -> Maybe (C.Colour Double)
    index' (x, y) = toColour . Ma.pixelColor <$> Ma.index image (Ma.Ix2 y x)

getTriangleAverageRGB :: Ma.Image Ma.S (Co.SRGB 'Co.Linear) Double -> (a, b) -> S.Set (P2 Int) -> Colour Double
getTriangleAverageRGB image (x', y') triangle = blendEqually $ getColorsInTriangle image (x', y') triangle

ptsBtween :: LineMXB -> LineMXB -> [(Int, Int)]
ptsBtween l1 l2 = concatMap rasterLine . noSingletons $ [startingX .. endingX]
  where
    startingX = max (startX l1) (startX l2)
    endingX = min (endX l1) (endX l2)

    rasterLine x = map (x,) $ range' (yAt l1 x) (yAt l2 x)

noSingletons :: [a] -> [a]
noSingletons [x] = []
noSingletons l = l

range' :: Int -> Int -> [Int]
range' a b = [(min a b) .. (max a b)]

yAt :: LineMXB -> Int -> Int
yAt (LineMXB {..}) x = round $ (m * (fromIntegral x)) + b

makeLine :: (Int, Int) -> (Int, Int) -> LineMXB
makeLine (x1, y1) (x2, y2) =
  LineMXB
    { m = slope,
      b = (fromIntegral y1) - (slope * (fromIntegral x1)),
      startX = min x1 x2,
      endX = max x1 x2
    }
  where
    slope =
      if x1 /= x2
        then (fromIntegral $ y1 - y2) % (fromIntegral $ x1 - x2)
        else fromIntegral . ceiling $ ((10.0 :: Double) ** 100.0)

data LineMXB = LineMXB
  { m :: Rational,
    b :: Rational,
    startX :: Int,
    endX :: Int
  }
  deriving (Show, Ord, Eq)
